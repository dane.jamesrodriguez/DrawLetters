﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrawLetters
{
    class Program
    {
        static void Main(string[] args)
        {
            IDisplayOutput letterToDraw = null;
            int letterSize = 0;

            Console.WriteLine("Enter a character [ O, X, Y, Z ]: ");
            letterToDraw = LetterInput(); //gets user input and returns a new object based on the letter input

            Console.WriteLine("Enter a non-negative odd integer: ");
            letterSize = NumberInput(); //gets user input and returns size after input validation

            letterToDraw.DisplayOutput(letterSize); //runs method of the object to draw the letter

            Console.Write("Thank you very much!");
            Console.ReadKey();                        
        }
        
        public static int NumberInput()
        {
            int number = 0;
            bool numberValid = false;

            while (!numberValid)
            {
                string numberTemp = Console.ReadLine();
                if (int.TryParse(numberTemp, out number))
                {
                    if (number % 2 != 0 && number > 0)
                    {
                        numberValid = true;
                        return number;
                    }
                    else Console.WriteLine("Please enter a non-negative odd integer");
                }
                else Console.WriteLine("Please enter a non-negative odd integer");
            }
            return 0;
        }
        public static IDisplayOutput LetterInput()
        {
            string letter = "";
            bool letterValid = false;
            IDisplayOutput letterInput = null;

            while (!letterValid)
            {
                letter = Console.ReadLine();

                switch (letter)
                {
                    case "O":
                        letterValid = true;
                        letterInput = new LetterO();
                        break;
                    case "X":
                        letterValid = true;
                        letterInput = new LetterX();
                        break;
                    case "Y":
                        letterValid = true;
                        letterInput = new LetterY();
                        break;
                    case "Z":
                        letterValid = true;
                        letterInput = new LetterZ();
                        break;
                    default:
                        letterValid = false;
                        Console.WriteLine("Please choose from the characters above");
                        break;
                }
            }            
            return letterInput;
        }

        //test method 
        //public static void TestDraw(int size) positive odd numbers only pls
        //{
        //    IDisplayOutput testO = new LetterO();
        //    IDisplayOutput testX = new LetterX();
        //    IDisplayOutput testY = new LetterY();
        //    IDisplayOutput testZ = new LetterZ();

        //    testO.DisplayOutput(size);
        //    testX.DisplayOutput(size);
        //    testY.DisplayOutput(size);
        //    testZ.DisplayOutput(size);
        //}
    }

    public interface IDisplayOutput
    {
        void DisplayOutput(int size);
    }

    public class LetterX : IDisplayOutput
    {
        public void DisplayOutput(int size)
        {
            Console.WriteLine($"Drawing letter X with size of {size}");

            int counter = 1;
            for (int i = 0; i < size; i++)
            {                
                for (int j = 0; j < size; j++)
                {
                    if (j == i || j == size - counter)
                    {
                        Console.Write("*");
                    }                        
                    else Console.Write(" ");
                }
                counter++;
                Console.Write("\n");
            }
        }
    }
    public class LetterO : IDisplayOutput
    {
        public void DisplayOutput(int size)
        {
            Console.WriteLine($"Drawing letter O with size of {size}");

            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == 0 || i == size - 1 || j == 0 || j == size - 1)
                    {
                        Console.Write("*");
                    }
                    else Console.Write(" ");
                }
                Console.Write("\n");
            }
        }
    }
    public class LetterY : IDisplayOutput
    {
        public void DisplayOutput(int size)
        {
            Console.WriteLine($"Drawing letter Y with size of {size}");

            int counter = 1;
            int midpoint = (1 + size)/ 2;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if ( (j == i || j == size - counter) && i < midpoint )
                    {
                        Console.Write("*");
                    }
                    else if ( i >= midpoint && j == midpoint - 1)
                    {
                        Console.Write("*");
                    }                    
                    else Console.Write(" ");
                }
                counter++;
                Console.Write("\n");
            }
        }
    }
    public class LetterZ : IDisplayOutput
    {
        public void DisplayOutput(int size)
        {
            Console.WriteLine($"Drawing letter Z with size of {size}");
                       
            int counter = size - 1;
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    if (i == 0 || i == size - 1 || j == counter)
                    {
                        Console.Write("*");
                    }                        
                    else Console.Write(" ");
                }
                counter--;
                Console.Write("\n");
            }
        }
    }

    
}
